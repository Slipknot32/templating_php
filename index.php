<?php

require_once 'vendor/autoload.php'; //composer 


$loader = new \Twig\Loader\FilesystemLoader(__DIR__. '/template');
$twig = new \Twig\Environment($loader, [
    'cache' => false,
]);

$faker = Faker\Factory::create();

echo $twig->render('index.html',[
'company' => $faker-> company,
'phrase' => $faker->sentence($nbWords = 6, $variableNbWords = true),
'productname' => $faker-> emoji,
'productmateriel' => $faker-> bs,
'url' => $faker-> url, 
'price' => $faker->randomFloat($nbMaxDecimals = 2, $min = 10, $max =150),
'username' => $faker-> userName,
'job' => $faker-> jobTitle,
'email' => $faker-> email, 
'phone' => $faker-> tollFreePhoneNumber,
'rue' => $faker-> streetAddress, 
'code' => $faker-> postcode,
'ville' => $faker-> city,
'color' => $faker-> colorName,

]);?>